from .app import manager, db
import yaml
# import des modeles
from .models import Groupe, Albums
from.models import User, load_user
from hashlib import sha256

@manager.command
def loaddb(filename):
    '''Creates the tables and populates them with data.'''

    # création de toutes les tablesdb.create_all()
    db.create_all()

    # chargement de notre jeu de données

    albums = yaml.load(open(filename))



    # premier passe: création de tous les auteurs
    groupes = {}
    for b in albums:
        a = b["by"]
        if a not in groupes:
            o = Groupe(name=a)
            #On ajoute l'objet o à la base:
            db.session.add(o)
            groupes[a] = o
    db.session.commit()
    #deuxième pass: création de tous les albums
    for b in albums:
        a = groupes[b["by"]]
        o = Albums(id = b["entryId"],
        title = b["title"],
        img	= b["img"],
        groupe_id = a.id)
        db.session.add(o)
    db.session.commit()

@manager.command
def syncdb():
    '''Creates all missing tables.'''
    db.create_all()






@manager.command
def newuser(username, password):
    '''Adds a new user.'''
    if load_user(username) is not None:
        print('Utilisateur existant !')
    else:
        m = sha256()
        m.update(password.encode())
        u = User(username=username, password=m.hexdigest())
        db.session.add(u)
        db.session.commit()
@manager.command
def passwd(username, password):
    '''Change a user password'''
    m = sha256()
    m.update(password.encode())
    u=load_user(username)
    u.password=m.hexdigest()
    db.session.commit()
