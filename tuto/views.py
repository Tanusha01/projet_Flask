from flask import url_for, redirect
from.app import db
from .models import Albums
from flask.ext.wtf import Form
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired
from .app import app
from flask import render_template
from .models import get_sample, get_album, get_genre_search, get_groupe_search, get_album_search,get_all_albums,get_all_groupes
from wtforms import PasswordField
from .models import User
from hashlib import sha256
from flask.ext.login import login_user, current_user
from flask import request
from flask.ext.login import logout_user
from flask.ext.login import login_required



@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))

# @app.route("/search",methods = ['POST'])
# def search():
# 	if not g.search_form.validate_on_submit():
# 		return redirect(url_for('home'))
# 	return redirect(url_for('seach_results', query = g.seache_form.seach.data))
#
# @app.route('/search_results/<query>')
# def search_results(query):
# 	results = Post.query.whoosh_seach(query,MAX_SEARCH_RESULTS).all()
# 	return redirect(url_for('home'))
# 	return redirect(url_for('seach_results', query = g.seache_form.seach.data))


#
@app.route("/search_resalts",methods = ('POST',))
def search_resalts():
	s = Search()
	if s.validate_on_submit():
		return redirect(url_for('home'))
	return redirect(url_for('recherche'))

@app.route("/search",methods = ('POST',))
def search():
	a = get_album_search(request.form['name'])
	g = get_groupe_search(request.form['name'])
	gr = get_genre_search(request.form['name'])

	if(a is None):
		a = []
	if(g is None):
		g = []
	if(gr is None):
		gr = []

	print(a)
	print(g)
	print(gr)
	return render_template(
		"recherche.html",
 		albums=a, groupe=g, genre =gr)



class LoginForm(Form):
	username = StringField('Username')
	password = PasswordField('Password')

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None


class AlbumsForm(Form):
	id = HiddenField('id')
	name = StringField('Nom', validators=[DataRequired()])



class NewAlbumsForm(Form):
	title = StringField('Title', validators=[DataRequired()])


@app.route("/")
def home():
	return render_template(
    "home.html",
    title="Music Groupes",
    albums = get_sample())

@app.route("/album/")
def album():
	f=NewAlbumsForm()
	return render_template(
    "album.html",
    title="All albums",
    albums = get_all_albums(),
	form=f)

@app.route("/group/")
def groupe():
	return render_template(
    "group.html",
    title="All groupes",
    groupes = get_all_groupes())


@app.route("/edit/albums/<int:id>")
@login_required
def edit_author(id):
	a = get_albums(id)
	f = AlbumsForm(id=a.id, name=a.name)
	return render_template(
		"edit-albums.html",
		albums=a, form=f)

@app.route("/edit/album/<int:id>")
def edit_albums(id):
	a = get_album(id)
	f = AlbumsForm(id=a.id, name=a.title)
	return render_template(
		"edit-albums.html",
		albums=a, form=f)

@app.route("/save/albums/", methods=("POST",))
def save_albums():
	a = None
	f = AlbumsForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_album(id)
		a.title = f.name.data
		db.session.commit()
		return redirect(url_for('view_album_2', id=a.id))
	a = get_album(int(f.id.data))#aucun echeck
	return render_template(
		"edit-albums.html",
		albums=a, form=f)






@app.route("/edit/groupes/<int:id>")
@login_required
def edit_groupes(id):
	a = get_groupe(id)
	f = GroupeForm(id=a.id, name=a.name)
	return render_template(
		"edit-groupes.html",
		albums=a, form=f)

@app.route("/save/groupes/", methods=("POST",))
def save_groupes():
	a = None
	f = GroupeForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_groupe(id)
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for('one_groupe', id=a.id))
	a = get_groupe(int(f.id.data))#aucun echeck
	return render_template(
		"edit-groupes.html",
		albums=a, form=f)

@app.route("/album/<int:id>", methods=("GET",))#je veux ce  que la route recupere toutes les id d'albumes
#l'adress qui va afficher dans le barre
def view_album(id):
	f = NewAlbumsForm()
	a = get_album(id)
	return render_template(
		"album.html",
		album=a, form=f)#Je veux afficher un album

@app.route("/edit/album/<int:id>", methods=("GET",))#je veux ce  que la route recupere toutes les id d'albumes
#l'adress qui va afficher dans le barre
def view_album_2(id):
	f = NewAlbumsForm()
	a = get_album(id)
	return render_template(
		"edit-albums.html",
		album=a, form=f)#Je veux afficher un album

@app.route("/newalbum/", methods=("POST",))
def new_albums():
	f = NewAlbumsForm()
	message = None
	if f.validate_on_submit():
		print(f.title.data)
		a = Albums(title=f.title.data)
		db.session.add(a)
		db.session.commit()
		# return redirect(url_for('view_album', id=a.id))
		message = "Album ajouté avec succés"
	return render_template(
		"album.html",
		albums = get_all_albums(), form=f, message=message)


@app.route("/seul_album/<int:id>", methods=("GET",))
def seul_album(id):
	a = get_album(id)
	return render_template(
		"seul_album.html",
		album=a )



@app.route("/login/", methods=("POST",))
def login():
	f = LoginForm()
	if f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			return redirect(url_for("home"))
	return render_template(
		"login.html",
		form=f)
