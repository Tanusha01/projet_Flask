from flask import Flask
from flask.ext.bootstrap import Bootstrap
import os.path
from flask.ext.login import LoginManager


app = Flask(__name__)
app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

app.config['SECRET_KEY'] = "7d992e34-1ab0-4558-80de-6378d9204158"
login_manager = LoginManager(app)
login_manager.login_vieu = "login"


from flask.ext.script import Manager
manager = Manager(app)

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../album.db'))
db = SQLAlchemy(app)
